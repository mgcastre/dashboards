## Open all available data
fPath <- "../../data/processed/"
fName <- "allProfilesTimeSeriesAndLocation.parquet"
allData <- arrow::read_parquet(
  file = here::here(paste0(fPath, fName)), 
  as_data_frame = FALSE   # keep as parquet table to keep back-end lighter
)

# Filter out only salinity data
salinityData <- allData %>% 
  dplyr::filter(Sensor == "SALT") %>% 
  dplyr::arrange(Date, Station_ID) %>% 
  dplyr::collect() %>% 
  data.frame()

## Select all points to plot
saltPoints <- salinityData %>% 
  dplyr::select(Station_ID, Latitude, Longitude) %>% 
  dplyr::arrange(Station_ID) %>% 
  dplyr::distinct()

# Filter data according to dates
date_range <- list(as.Date("2016-04-01"), as.Date("2016-04-30"))
filteredData <- salinityData %>%
  dplyr::filter(Date >= date_range[1] & Date <= date_range[2])

# Filter points to plot
filteredPoints <- filteredData %>% 
  dplyr::select(Station_ID, Latitude, Longitude, Date) %>% 
  dplyr::distinct()

# Try to plot markers
leaflet(filteredPoints) %>% addTiles(group = "OSM") %>% 
  addMarkers(lng = ~Longitude, lat = ~Latitude,
             label = ~Station_ID, layerId = ~Station_ID)