# Sourcecode for dash app to display average salinity profile data
# Salinity data from the Panama Canal Authority (ACP)
# M. G. Castrellón | 16/05/2024

# Required Libraries and Packages
import re
import os
import dash
import h3pandas
from dash import dcc
from dash import html
from dash.dependencies import Input, Output
from branca.colormap import linear
import plotly.express as px
import geopandas as gpd
import pandas as pd
import folium

# Load Raw Data
pathRel = './data/interim/eda'
fileName = 'salinityExceedanceFrequency.csv'
freq = pd.read_csv(os.path.join(os.getcwd(), pathRel, fileName))

def create_hexagrid(df, var, level=8):
    g_level = f"h3_{level:02}"
    gdfh3 = df.h3.geo_to_h3(level) \
        .groupby(g_level)[[var]].mean() \
        .h3.h3_to_geo_boundary()
    return gdfh3

# Define function to generate Folium map
def generate_map(data, var):
    # Create a Folium map centered at a specific location
    map_obj = folium.Map(location=[9.12, -79.79], zoom_start=10)

    # Define a colormap based on salinity values
    salinity_colormap = linear.YlOrRd_09.scale(min(data[var]), max(data[var]))

    # Add polygons
    for _, r in data.iterrows():    
        hexagon = gpd.GeoSeries(r["geometry"]).to_json()
        popup_content = f"{var}: {r[var]}"
        salinity_color = salinity_colormap(r[var])
        style_function = lambda x, color = salinity_color: {"fillColor": color, "fillOpacity": 0.7, "color":"black", "weight":1}
        geo_j = folium.GeoJson(data=hexagon, style_function=style_function, tooltip=popup_content)
        geo_j.add_to(map_obj)

    # Add colormap legend
    salinity_colormap.add_to(map_obj)

    # Return map object
    return map_obj

# Initialize the Dash app
app = dash.Dash(__name__)

# Define the layout of the app
app.layout = html.Div([
        dcc.Dropdown(  # Dropdown to select variable for plotting on map
            id='variable-dropdown',
            options=[{'label': 'Salinity Concentration', 'value': 'Salinity'}, \
                     {'label': 'Frequency of Exceedance (0.18)', 'value': 'Above_Limit_1'},
                     {'label': 'Frequency of Exceedance (0.42)', 'value': 'Above_Limit_2'}],
            placeholder="Select a variable to plot",
            value='Salinity', # Default value
        ),
        dcc.Dropdown(  # Dropdown for filtering by category 1
            id='period-dropdown',
            options=['Pre-Expansion', 'Post-Expansion'],
            placeholder="Select a time period",
            value='Pre-Expansion', # Default value
        ),
        dcc.Dropdown(  # Dropdown for filtering by category 2
            id='depth-dropdown',
            options=['0-5', '5-15', '>15', 'All'],
            placeholder="Select a depth group",
            value='0-5', # Default value
        ),
        html.Div(id='map-container')
])

# Define callback to update the map based on filter selection
@app.callback(
    Output('map-container', 'children'),
    [Input('variable-dropdown', 'value'),
     Input('period-dropdown', 'value'),
     Input('depth-dropdown', 'value')]
)
def update_markers(selected_variable, selected_period, selected_depth):

    # Filter data according to dronwdown menu options
    condition01 = (freq['Period'] == selected_period)
    condition02 = (freq['Depth_Group'] == selected_depth)
    filtered_df = freq.loc[condition01 & condition02, :] \
        .copy(deep=True).reset_index()
    
    # Filter data according to dropdown menu options

    ## Selected period
    filtered_df = freq.loc[freq['Period'] == selected_period, :].copy(deep=True)
    
    ## Selected depth
    if selected_depth == 'All':
        filtered_df = filtered_df.groupby(['Station_ID','Period'], observed=True) \
            .mean(numeric_only=True).reset_index() 
    else:
        filtered_df = filtered_df.loc[filtered_df['Depth_Group'] == selected_depth, :]

    
    # Generate hexagons
    hexagons = create_hexagrid(df=filtered_df, var=selected_variable)

    # Generate Folium map based on filtered data
    folium_map = generate_map(data=hexagons, var=selected_variable)

    # Convert Folium map to HTML
    folium_map_html = folium_map._repr_html_()

    return html.Iframe(srcDoc=folium_map_html, style={'width': '100%', 'height': '600px'})

# Run the app
if __name__ == '__main__':
    app.run_server(debug=True)
