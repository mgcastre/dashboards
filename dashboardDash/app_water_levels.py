# Sourcecode for dash app to display Gatun Lake's water level data
# Water level data from the Panama Canal Authority (ACP)
# M. G. Castrellón | 10 May 2024

# Load libraries
import os
import dash
from dash import dcc
from dash import html
from dash.dependencies import Input, Output
import plotly.express as px
import pandas as pd

# Define Working Directory
os.chdir('D:/SURFdrive/Projects/DataDriven_Model')

# Load and Prepare Data

## Load data
fName = 'GatunLakeLevels_2000_2023.csv'
pathRel = './data/raw/hydromet/lakelevels'
pathFull = os.path.join(os.getcwd(), pathRel, fName)
table_info = pd.read_csv(pathFull, nrows=5, skiprows=1)
gatun_levels = pd.read_csv(pathFull, header=None, skiprows=5)
col_headers = table_info.columns.array[1:].tolist()

## Add header
headers = ['Date_Time']
headers.extend(col_headers)
gatun_levels.columns = headers

## Pivot table and convert to datetime
gatun_levels = gatun_levels.melt(id_vars='Date_Time', var_name='Station_ID', value_name='Value')
gatun_levels['Date_Time'] = pd.to_datetime(gatun_levels['Date_Time'])


# Initialyze Dash App
app = dash.Dash(__name__)

## Define Layout
app.layout = html.Div([
    dcc.Graph(id='water-level-graph'),
    dcc.Dropdown(
        id='location-dropdown',
        options=[{'label': i, 'value': i} for i in gatun_levels['Station_ID'].unique()],
        value=gatun_levels['Station_ID'].unique()[0]
    )
])

## Define Callback
@app.callback(
    Output('water-level-graph', 'figure'),
    [Input('location-dropdown', 'value')]
)

def update_graph(selected_location):
    filtered_df = gatun_levels.loc[gatun_levels['Station_ID'] == selected_location, :]
    fig = px.line(filtered_df, x='Date_Time', y='Value', 
                  title='Gatun Lake Water Levels (15-min data)', 
                  labels={'Value': 'Water Level (ft)', 'Date_Time': 'Date Time'})

    return fig

if __name__ == '__main__':
    app.run_server(debug=True)